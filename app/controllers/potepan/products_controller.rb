class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_COUNT = 4

  def show
    @product = Spree::Product.find(params[:id])
    taxons = Spree::Product.in_taxons.includes(master: [:default_price, :images])
    @related_products = taxons.distinct.where.not(id: @product.id).limit(RELATED_PRODUCTS_COUNT)
  end
end
