require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  let!(:taxonomy)   { create(:taxonomy, name: "Categories") }
  let!(:taxon)      { create(:taxon,    name: "T-shirts", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product)    { create(:product,  name: "Nike", taxons: [taxon]) }

  before do
    get potepan_category_path(taxon.id)
  end

  it "リクエストが成功した場合のレスポンスが返ってくる" do
    expect(response).to have_http_status "200"
  end

  it "showテンプレートがレンダリングされる" do
    expect(response).to render_template :show
  end

  it "インスタンス変数に期待する値が入っている" do
    expect(assigns(:taxonomies).first).to eq(taxon.root)
    expect(assigns(:taxon)).to eq(taxon)
    expect(assigns(:products)).to eq(taxon.products)
  end
end
