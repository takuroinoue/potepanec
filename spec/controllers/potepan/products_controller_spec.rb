require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let!(:taxon) { create(:taxon, name: "T-shirts") }
  let!(:product) { create(:product, name: "sample", price: "12.34", taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

  before do
    get potepan_product_path(product.id)
  end

  it "商品名を正しく返している" do
    expect(response.body).to include "sample"
  end

  it "商品の表示価格を正しく返している" do
    expect(response.body).to include "$12.34"
  end

  it "リクエストが成功した場合のレスポンスが返ってくる" do
    expect(response).to have_http_status "200"
  end

  it "インスタンス変数(product)に期待する値が入っている" do
    expect(assigns(:product)).to eq product
  end

  describe "インスタンス変数(related_products)について" do
    it "4つのデータが入る" do
      expect(assigns(:related_products).size).to eq 4
    end

    it "データと、並び順が一致する" do
      expect(assigns(:related_products)).to eq related_products
    end
  end

  it "showテンプレートがレンダリングされる" do
    expect(response).to render_template :show
  end
end
