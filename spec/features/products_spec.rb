require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let!(:taxon) { create(:taxon, name: "T-shirts") }
  let!(:product) { create(:product, name: "sample", price: "12.34", taxons: [taxon]) }
  let!(:products) { create_list(:product, 3, name: "variation", price: "99.99", taxons: [taxon]) }
  let!(:one_product) { create(:product, name: "last", price: "11.11", taxons: [taxon]) }
  let!(:product5) { create(:product, name: "Five", price: "55.55", taxons: [taxon]) }

  scenario "商品の個別詳細ページのメイン要素を検証する" do
    visit potepan_product_path(product.id)
    # メインの商品名と価格と解説が表示されている
    expect(page).to have_content "sample"
    expect(page).to have_content "12.34"
    expect(page).to have_content "As seen on TV!"
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(product.taxons.ids.first)
  end

  scenario "商品の個別詳細ページの関連商品を検証する" do
    visit potepan_product_path(product.id)
    # 関連商品の商品名と価格が表示されている
    expect(find(".RelatedProduct")).to have_content "variation"
    expect(find(".RelatedProduct")).to have_content "99.99"
    expect(find(".RelatedProduct")).to have_content "last"
    expect(find(".RelatedProduct")).to have_content "11.11"
    # ４つの商品がある
    within ".RelatedProduct" do
      expect(page).to have_selector ".productBox", count: 4
    end
    # ５つ目の商品は含まれない
    expect(find(".RelatedProduct")).not_to have_content "Five"
    expect(find(".RelatedProduct")).not_to have_content "55.55"
    # 現在いる商品詳細ページの商品は含まれない
    expect(find(".RelatedProduct")).not_to have_content "sample"
    expect(find(".RelatedProduct")).not_to have_content "12.34"
    click_link "last"
    expect(current_path).to eq potepan_product_path(one_product.id)
  end
end
