require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxonomy) { create(:taxonomy, name: "Categories") }
  let!(:taxon) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:other_taxon) { create(:taxon, name: "Mugs", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:one_product) { create(:product, name: "One_Mug", price: "99.99", taxons: [other_taxon]) }
  let!(:two_product) { create(:product, name: "Shoes", price: "10.00") }
  let!(:product) { create(:product, name: "First_BAG", price: "12.34", taxons: [taxon]) }
  let!(:product2) { create(:product,  name: "Second_BAG", price: "56.78", taxons: [taxon]) }
  let!(:product3) { create(:product,  name: "Third_BAG", price: "90.12", taxons: [taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  feature "Taxonomies" do
    scenario "カテゴリー名をクリックするとその分類と商品数が表示される" do
      click_link "Categories"
      expect(page).to have_content "Bags"
      expect(page).to have_content "(3)"
      expect(page).to have_content "Mugs"
      expect(page).to have_content "(1)"
    end

    scenario "カテゴリーの分類に紐づく商品がない場合、商品は表示されない" do
      click_link "Categories"
      click_link "Mugs"
      expect(page).to have_content "One_Mug"
      expect(page).to have_content "99.99"
      expect(page).not_to have_content "Shoes"
      expect(page).not_to have_content "10.00"
    end

    scenario "分類のリンクが正しいか検証する" do
      click_link "Bags"
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    scenario "商品が正しく表示されている" do
      expect(page).to have_content "First_BAG"
      expect(page).to have_content "Second_BAG"
      expect(page).to have_content "Third_BAG"
      expect(page).to have_content "12.34"
      expect(page).to have_content "56.78"
      expect(page).to have_content "90.12"
    end

    scenario "商品名のリンクが正しいか検討する" do
      click_link "First_BAG"
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
